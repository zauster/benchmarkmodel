#!/bin/bash


dir=$1
i=$2

echo "Starting experiment $i..."
# sleep 20

## If just a single simulation experiment, but in parallel.
## Probably not advisable, because where would the parallel
## experiments write their outputs?

## java -jar build/libs/benchmark-0.1.0.jar -Djabm.config=model/mainBenchmark_light.xml

## Run the simulations of one experiment as given in the
## experiment.properties file in a directory
java -jar -Djabm.config=model/mainBenchmark_zauster.xml -Djabm.propertyfile=$dir/experiment$i/experiment.properties build/libs/benchmark-0.1.0.jar

echo "Finished experiment in $dir/experiment$i"

