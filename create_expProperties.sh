#!/bin/bash

expfile=$1
expdesc=$2

## generate all experiment files
java -jar -Djabm.varfile=$1 -Djabm.config=model/mainBenchmark_zauster.xml build/libs/benchmark-0.1.0.jar

## create that directory
mkdir data/$expdesc

## move them to a different directory
mv data/experiment* data/$expdesc/

## replace the directory in the experiment.properties files
for file in data/$expdesc/experiment*/experiment.properties
do
    # echo $file
    sed -i "s+data/experiment+data/${expdesc}/experiment+g" $file
done
