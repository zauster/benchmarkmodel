This extension is a collection of additional classes (such as
reports and other utilities) that I have collected over time but
cannot be subsumed under a common topic other than "misc".
