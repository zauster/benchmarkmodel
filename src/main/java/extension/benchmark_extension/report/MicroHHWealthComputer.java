/*
 * JMAB - Java Macroeconomic Agent Based Modeling Toolkit
 * Copyright (C) 2013 Alessandro Caiani and Antoine Godin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 */
package benchmark_extension.report;

import java.util.Map;
import java.util.TreeMap;

import benchmark.StaticValues;
import benchmark_extension.ExtendedStaticValues;
import benchmark.agents.Households;
import jmab.population.MacroPopulation;
import jmab.report.AbstractMicroComputer;
import jmab.report.MicroMultipleVariablesComputer;
import jmab.simulations.MacroSimulation;
import net.sourceforge.jabm.Population;
import net.sourceforge.jabm.agent.Agent;

/**
 * @author Alessandro Caiani and Antoine Godin
 * This computer computes the households wealth.
 */
public class MicroHHWealthComputer extends AbstractMicroComputer implements MicroMultipleVariablesComputer {

  private int householdId;

  private int WealthComponent;

  /* (non-Javadoc)
   * @see jmab.report.MicroMultipleVariablesComputer#computeVariables(jmab.simulations.MacroSimulation)
   */
  @Override
  public Map<Long, Double> computeVariables(MacroSimulation sim) {
    MacroPopulation macroPop = (MacroPopulation) sim.getPopulation();
    Population pop = macroPop.getPopulation(householdId);
    TreeMap<Long, Double> result = new TreeMap<Long, Double>();

    switch (WealthComponent) {
    case ExtendedStaticValues.HHWEALTHREPORT_ID:
      for (Agent i : pop.getAgents()) {
        Households households = (Households) i;
        double tmp = 0;
        tmp += households.getCashAmount();
        tmp += households.getDepositAmount();
        result.put(households.getAgentId(), tmp);
      }
      break;
    case ExtendedStaticValues.HHCASHAMOUNTREPORT_ID:
      for (Agent i : pop.getAgents()) {
        Households households = (Households) i;
        double tmp = 0;
        double[][] bs = households.getNumericBalanceSheet();
        tmp += bs[0][StaticValues.SM_CASH];
        tmp -= bs[1][StaticValues.SM_CASH];
        // tmp += households.getCashAmount();
        result.put(households.getAgentId(), tmp);
      }
      break;
    case ExtendedStaticValues.HHDEPOSITAMOUNTREPORT_ID:
      for (Agent i : pop.getAgents()) {
        Households households = (Households) i;
        double tmp = 0;
        double[][] bs = households.getNumericBalanceSheet();
        tmp += bs[0][StaticValues.SM_DEP];
        tmp -= bs[1][StaticValues.SM_DEP];
        // tmp += households.getDepositAmount();
        result.put(households.getAgentId(), tmp);
      }
      break;
    }
    return result;
  }

  /**
   * @return the wealthComponent
   */
  public int getWealthComponent() {
    return WealthComponent;
  }

  /**
   * @param wealthComponent the wealthComponent to set
   */
  public void setWealthComponent(int wealthComponent) {
    this.WealthComponent = wealthComponent;
  }

  /**
   * @return the consumptionFirmsId
   */
  public int getHouseholdId() {
    return householdId;
  }

  /**
   * @param householdId the consumptionFirmsId to set
   */
  public void setHouseholdId(int householdId) {
    this.householdId = householdId;
  }
}
