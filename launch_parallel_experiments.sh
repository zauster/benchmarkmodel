#!/bin/bash

dir=$1
max_tasks=$2

# running_tasks=0

## Use the following line to find the number of
## experiment-folders in a directory
number_experiments="$(find $dir -type d -name experiment\* | wc -l)"
echo "Number directories found: $number_experiments"

for i in $(eval echo {1..$number_experiments})
do
    ## Try also:
    ## pag 'java -jar' | wc -l   -> subtract 1 (for the grep)
    # while (("$(ps -C java --no-headers | wc -l)" >= $max_tasks ))
    running_tasks="$(ps aux | grep -v 'grep' | grep 'java -jar' | wc -l)"
    echo "Running tasks: $running_tasks"
    while ((running_tasks >= $max_tasks))
    do
        ## sleep for two minutes
        sleep 120
        running_tasks="$(ps aux | grep -v 'grep' | grep 'java -jar' | wc -l)"
        # echo " ... waiting for simulations to finish: $running_tasks"
    done

    echo "Running experiment $i of $number_experiments"
    ./launch_single_experiment.sh $dir $i &
    sleep 5

done
